# first parsec data file
FDF_VERSION = 1
FDF_INDENT_SIZE = 2

class fdf_comment:
    def __init__(self, value=""):
        self.value = value

class fdf_include:
    def __init__(self, value=""):
        self.value = value

def is_special_node(node):
    if type(node) is fdf_comment:
        return True
    if type(node) is fdf_include:
        return True
    return False

def serialize_fdf_node(node, indent):
    indent_string = " " * FDF_INDENT_SIZE * indent
    t = type(node)
    if t is int:
        return "%d" % node
    elif t is float:
        r = "%f" % node
        r = r.rstrip('0')
        if r.endswith("."):
            r += "0"
        return r
    elif t is str:
        return "\"%s\"" % node.replace('"', '\\"').replace("'", "\\'")
    elif t is bool:
        if node:
            return "true"
        return "false"
    elif t is dict:
        result = []
        if indent > 0:
            result.append("")
        for key in node:
            if is_special_node(node[key]):
                result.append(serialize_fdf_node(node[key], indent))
            else:
                result.append("%s%s: %s" % (
                    indent_string,
                    key,
                    serialize_fdf_node(node[key], indent + 1)
                ))
        return "\n".join(result)
    elif t is list or t is tuple:
        short_vec = True
        vecdata = []
        for v in node:
            if type(v) is int or type(v) is float or type(v) is bool:
                vecdata.append(serialize_fdf_node(v, 0))
            else:
                short_vec = False
        if short_vec:
            return "{%s}" % " ".join(vecdata)
        else:
            index = 0
            result = []
            if indent > 0:
                result.append("")
            for v in node:
                if is_special_node(v):
                    result.append(serialize_fdf_node(v, indent))
                else:
                    result.append("%s%d: %s" % (
                        indent_string,
                        index,
                        serialize_fdf_node(v, indent + 1)
                    ))
                index += 1
            return "\n".join(result)
    elif t is fdf_comment:
        return "%s# %s" % (indent_string, node.value)
    elif t is fdf_include:
        return "%s#include %s" % (indent_string, node.value)


def count_indent(line):
    indent = 0
    for c in line:
        if c == ' ':
            indent += 1
        else:
            break
    return indent


def skip_whitespace(line):
    index = 0
    for c in line:
        if c == ' ':
            index += 1
        else:
            break
    return line[index:]

def parse_value(typ, data):
    if typ == "string":
        value = ""
        is_esc = 0
        for c in data:
            if c == '\\':
                is_esc = 1
                continue
            if c == "'" or c == '"':
                if is_esc <= 0:
                    break
            is_esc -= 1
            value += c
        return value
    elif typ == "number":
        value = ""
        subtyp = "int"
        for c in data:
            if c not in "0123456789-.":
                break
            if c == ".":
                subtyp = "float"
            value += c
        if subtyp == "float":
            return float(value)
        else:
            return int(value)
    elif typ == "inline":
        value = []
        mode = 0
        index = -1
        for c in data:
            index += 1
            if c == "}":
                break
            if mode == 3:
                if c != ' ':
                    continue
                else:
                    mode = 0
            if mode == 0:
                if c == ' ':
                    continue
                else:
                    mode = 1
            if mode == 1:
                v = parse_value("number", data[index:])
                value.append(v)
                mode = 3
        return value


def parse_item(line):
    key = ""
    value = {}
    typ = "struct"
    mode = 0
    index = -1
    for c in line:
        index += 1
        if mode == 0:
            if c != ' ':
                mode = 1
        if mode == 1:
            if c == ':':
                mode = 2
                continue
            else:
                key += c
        if mode == 2:
            if c == " ":
                continue
            if c in "0123456789-.":
                typ = "number"
                value = parse_value("number", line[index:])
                break
            if c in ('"', "'"):
                typ = "string"
                value = parse_value("string", line[index+1:])
                break
            if c == "{":
                typ = "inline"
                value = parse_value("inline", line[index+1:])
                break
    return (key, value, typ)

# public methods

def load(fp):
    data = fp.read()
    return loads(data)

def loads(data):
    result = {}
    current_indent = 0
    stack = [result]
    lines = data.split("\n")
    for line in lines:
        indent_spaces = count_indent(line)
        indent = int(indent_spaces / FDF_INDENT_SIZE)
        while indent < current_indent:
            current_indent -= 1
            stack.pop()
        current_indent = indent
        line_no_white = skip_whitespace(line).replace("\n", "")
        if line_no_white.startswith("#"):
            continue
        key, value, typ = parse_item(line_no_white)
        stack[-1][key] = value
        if typ == "struct":
            current_indent = indent
            stack.append(stack[-1][key])
    return result

def dump(fp, source):
    data = dumps(source)
    fp.write(data)

def dumps(source):
    return serialize_fdf_node(source, 0)

