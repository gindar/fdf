# First Parsec Data format #

This is library for reading and writing FDF files.

### Features

 * similar to YAML but without some annoying features
 * intended for game entity definitions and other assets like models, prefabs etc.
 * used in First Parsec Game
 * import/include support
 * basic types: `string`, `int`, `float`, `bool`
 * only dict and inline array (intended for Vec3/Quat/Color etc.)

### Why another text file format?

 * FDF is more compact and readable than JSON
 * FDF is simpler than YAML
 * it's tailored for game assets 

```yaml
# comment
key:
  sub: 1
  sub2: "string"
  array: {1.0 2.0 3.0}
  subobject:
    key: 123
    key2: true
```

is parsed as

```json
{
  "key": {
    "sub": 1,
    "sub2": "string",
    "array": [1.0, 2.0, 3.0],
    "subobject": {
      "key": 123,
      "key2": true
    }
  }
}
```