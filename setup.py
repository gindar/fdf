from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name='fdataf',
    version='1.0.0',
    description='First Parsec Data file',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/gindat/fdf',
    author='Josef Vanzura',
    author_email='gindar@zamraky.cz',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
    ],
    keywords='firstparsec, YAML, development',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.6, <4',
    install_requires=[],
)
